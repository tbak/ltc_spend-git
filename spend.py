from blockcypher import create_unsigned_tx
from blockcypher import make_tx_signatures
from blockcypher import broadcast_signed_transaction

# xg5
inputs = [{'address': 'LUjMmbqvTymJVpDc8oS5XMdfbpxWU964M6'}, ]
outputs = [{'address': 'Ld82EYrHP8sk4PLkYzdoYZ86gkuUvfEwz1', 'value': 6018000000}]
unsigned_tx = create_unsigned_tx(inputs=inputs, outputs=outputs, coin_symbol='ltc', api_key='e3bbd1cc4de4b583eac5d485d632bc02')
print(unsigned_tx)

# Using our same unsigned_tx as before, iterate through unsigned_tx['tx']['inputs'] to find each address in order.
# Include duplicates as many times as they may appear:
privkey_list = ['6uQByuLYrfPPFMXeibSHrvWTmJ838uj13NgkrCNeue34CgaWXTh', ]
pubkey_list = ['0493590b0e970d7b4a949f825dc669256505d1cfcbe7407d133df54cba6f4990e8b4b1739b881ab8753cdf5d3776436e2eab5a9085d54934b100ff0333453dbe5e', ]
tx_signatures = make_tx_signatures(txs_to_sign=unsigned_tx['tosign'], privkey_list=privkey_list, pubkey_list=pubkey_list)
print(tx_signatures)

result = broadcast_signed_transaction(unsigned_tx=unsigned_tx, signatures=tx_signatures, pubkeys=pubkey_list, api_key='e3bbd1cc4de4b583eac5d485d632bc02', coin_symbol='ltc')
print(result)
